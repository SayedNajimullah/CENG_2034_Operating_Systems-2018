This week we see how to user space and kernel space interact each other by doing syscall calls in linux.

For example we made 2 different system calls. **Getpid** to see the id of the running process and **fork** to create a new child process. 

### Assignment 1:

* Create multiple child processes.
* Perform some operations with one of these.
* Kill the child process after the process is finished.

### Deadline February 23, 00:00  
#### It does not matter which git host you are using. But I will search in this way: 
#### githost.com/username/CENG_2034_Operating_Systems-2018
#### Make sure you follow this format !!! 

You can do it with the programming language you want. If there is a special case please write a comment line at the top of the code file. 